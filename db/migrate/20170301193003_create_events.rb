class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :eventtext
      t.integer :guest
      t.date :deadline

      t.timestamps null: false
    end
  end
end
