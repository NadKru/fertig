json.array!(@event_job_job_associations) do |event_job_job_association|
  json.extract! event_job_job_association, :id, :event_id, :predecessor_id, :successor_id
  json.url event_job_job_association_url(event_job_job_association, format: :json)
end
